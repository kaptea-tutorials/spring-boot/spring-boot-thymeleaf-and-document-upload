package com.kaptea.employee.service;

import com.kaptea.employee.entity.Department;
import java.util.List;
import java.util.Optional;

public interface DepartmentService {

    Department save(Department department);

    List<Department> findAll();

    Optional<Department> getDepartment(Long id);

    void delete(Long id);

}