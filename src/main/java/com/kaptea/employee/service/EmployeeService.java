package com.kaptea.employee.service;

import com.kaptea.employee.dto.UserDto;
import com.kaptea.employee.entity.Employee;

import java.util.List;

public interface EmployeeService {
	
	Employee save(Employee employee);

    List<Employee> findAll();

    Employee findByUsernameAndPassword(UserDto userDto);
}