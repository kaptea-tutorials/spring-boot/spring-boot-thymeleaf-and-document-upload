package com.kaptea.employee.repository;

import com.kaptea.employee.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByUsernameAndPasswordAndActive(String username, String password, boolean active);
}