package com.kaptea.employee.controller;

import com.kaptea.employee.entity.Employee;
import com.kaptea.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    EmployeeController(final EmployeeService employeeService){
        this.employeeService = employeeService;
    }

    // From controller im hiting employees url
    // Fetching list of employees
    @GetMapping("/employees")
    public String employees(Model model, Employee employee) {

        List<Employee> employees = employeeService.findAll();
        model.addAttribute("employees", employees);

        return "employee"; // Name of my html file
    }

    // Explaining create first
    @PostMapping("/create-new")
    public String createNewEmployee(@Valid Employee employee, BindingResult result, Model model) {

        employeeService.save(employee);

        List<Employee> employees = employeeService.findAll();
        model.addAttribute("employees", employees);

        return "employee";
    }
}
