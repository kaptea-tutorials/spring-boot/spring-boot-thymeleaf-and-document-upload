package com.kaptea.employee.controller;

import com.kaptea.employee.entity.Department;
import com.kaptea.employee.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    /**
     * @return all the departments available in the database
     */
    @GetMapping("department")
    public ResponseEntity<List<Department>> getDepartments() {
        List<Department> departments = departmentService.findAll();
        if (CollectionUtils.isEmpty(departments)) {
            throw new EntityNotFoundException("Department");
        }
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }

    /**
     * @param : save object to database and return the saved object
     * @return
     */

    @PostMapping("department")
    public ResponseEntity<Department> saveDepartment(@RequestBody Department department) {
        Department persistedObject = departmentService.save(department);
        return new ResponseEntity<>(persistedObject, HttpStatus.CREATED);
    }

    /**
     * @param id
     * @return return single department object by passing id
     */
    @GetMapping("department/{id}")
    private ResponseEntity<Department> getDepartment(@PathVariable long id) {
        Optional<Department> department = departmentService.getDepartment(id);
        if (!department.isPresent()) {
            throw new EntityNotFoundException(Department.class.getSimpleName());
        }
        return new ResponseEntity<>(department.get(), HttpStatus.OK);
    }

    /**
     * @param id
     * @return permanent deactivate of department by provided id | no permanent delete
     */
    @DeleteMapping("department/{id}")
    public ResponseEntity<Department> deleteDepartment(@PathVariable long id) {
        Optional<Department> department = departmentService.getDepartment(id);
        if (!department.isPresent()) {
            throw new EntityNotFoundException(Department.class.getName());
        }
        departmentService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * @param
     * @return to update the expense object
     */
    @PutMapping("department")
    public ResponseEntity<Department> updateDepartment(@Valid @RequestBody Department department) {
        departmentService.save(department);
        return new ResponseEntity<>(department, HttpStatus.OK);
    }
}